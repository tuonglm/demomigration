﻿// Copyright (C) 2000 Rivercrane Corporation.All Rights Reserved.
var contextMenuRegister = "選択した純正部品番号をウェビックで見積もる";
var contextMenuDeleteAll = "ウェビックに入力した純正部品をクリア";
var deleteMessage = "入力した純正部品を全て削除しますが、よろしいですか？";
var over20productCodeMessage = "純正部品番号の見積登録件数が、50件を超えました";
var webikePage = "https://www.webike.net/wbs/genuine-estimate-input.html";
//552,55,534,533,433
var regExpHonda = ["[A-z0-9]{5}-[A-z0-9]{5}-[A-z0-9]{2}"
				, "[A-z0-9]{5}-[A-z0-9]{5}"
				, "[A-z0-9]{5}-[A-z0-9]{3}-[A-z0-9]{4}"
				, "[A-z0-9]{5}-[A-z0-9]{3}-[A-z0-9]{3}"
				, "[A-z0-9]{4}-[A-z0-9]{3}-[A-z0-9]{3}"];
//3-5-2,5-5,3-5-2-2	
var regExpYamaha = ["[A-z0-9]{5}-[A-z0-9]{5}"
					, "[A-z0-9]{3}-[A-z0-9]{5}-[A-z0-9]{2}-[A-z0-9]{2}"
					, "[A-z0-9]{3}-[A-z0-9]{5}-[A-z0-9]{2}"];
//543,542,54,53
var regExpKawasaki = ["[A-z0-9]{5}-[A-z0-9]{4}-[A-z0-9]{3}"
					, "[A-z0-9]{5}-[A-z0-9]{4}-[A-z0-9]{2}"
					, "[A-z0-9]{5}-[A-z0-9]{4}"
					, "[A-z0-9]{5}-[A-z0-9]{3}"];
//55,553
var regExpSuzuki = ["[A-z0-9]{5}-[A-z0-9]{5}-[A-z0-9]{3}"
					, "[A-z0-9]{5}-[A-z0-9]{5}"];
//common: 553,552,55,543,542,54,534,533,53,433,3522,352
var regExpCommon = ["[A-z0-9]{5}-[A-z0-9]{5}-[A-z0-9]{3}"
					, "[A-z0-9]{5}-[A-z0-9]{5}-[A-z0-9]{2}"
					, "[A-z0-9]{5}-[A-z0-9]{5}"
					, "[A-z0-9]{5}-[A-z0-9]{4}-[A-z0-9]{3}"
					, "[A-z0-9]{5}-[A-z0-9]{4}-[A-z0-9]{2}"
					, "[A-z0-9]{5}-[A-z0-9]{4}"
					, "[A-z0-9]{5}-[A-z0-9]{3}-[A-z0-9]{5}"
					, "[A-z0-9]{5}-[A-z0-9]{3}-[A-z0-9]{4}"
					, "[A-z0-9]{5}-[A-z0-9]{3}-[A-z0-9]{3}"
					, "[A-z0-9]{5}-[A-z0-9]{3}"
					, "[A-z0-9]{4}-[A-z0-9]{3}-[A-z0-9]{3}"
					, "[A-z0-9]{3}-[A-z0-9]{5}-[A-z0-9]{2}-[A-z0-9]{2}"
					, "[A-z0-9]{3}-[A-z0-9]{5}-[A-z0-9]{2}"];
//using for take 1st webike page
var webikeTabId = 0;
var contexts = ["all", "selection"];
var limit = 50;
var del = false;

/**
 * @Author: rcv!LuongTuong
 * @Date: 12/25/2014
 * @Description:  Sync data from localStorage to Chrome.Storage
 */
function synDataLocalToWebike() {
	var channels = "";
	var sessionData = localStorage.getItem("itemData");
	if (sessionData) {
		channels = sessionData;
	}
	var dataArr = getSaveChanges().reverse();
	while (dataArr.length) {
		var elementArray = dataArr.pop();
		if (elementArray != null) {
			channels += elementArray + "::";
			if (dataArr.length == 1) {
				var data = dataArr.pop();
				if (data != null) {
					channels += data;
				}
			}
		}
	}
	
	if (del) {
		channels = 'del';
		del = false;
	}
	
	localStorage.setItem("itemData", channels);
	
	chrome.storage.local.set({
		'channels': channels
	});
}


function removeDataLocalToWebike() {
	var channels = "";
	chrome.storage.local.set({
		'channels': channels
	});
}

/**
 * @Author: rcv!LuongTuong
 * @Date: 12/17/2014
 * @Description:  Declare event catch key shortcut
 */
chrome.commands.onCommand.addListener(function(command) {
	if (command == "redirect") {
//		redirectPage();
	} else if (command == "register") {
		/* Inject the code into all frames of the active tab */
		chrome.tabs.executeScript({
			code: jsCodeStr,
			allFrames: true
		}, function(selectedTextPerFrame) {
			if (chrome.runtime.lastError) {
				/* Report any error */
				alert('ERROR:\n' + chrome.runtime.lastError.message);
			} else if (selectedTextPerFrame.length > 0) {
				//if (selectedTextPerFrame.length == 1) {
				//	//フレームなし
				//	if (selectedTextPerFrame[0].length > 0 && (typeof(selectedTextPerFrame[0]) === 'string')) {
				//		addItemToStorageByHotKey(checkRegExp(selectedTextPerFrame[0]));
				//	}
				//} else {
				//	//フレームあり
					for (var i = 0; i < selectedTextPerFrame.length; i++) {
						if (selectedTextPerFrame[i].length > 0 && (typeof(selectedTextPerFrame[i]) === 'string')) {
							addItemToStorageByHotKey(checkRegExp(selectedTextPerFrame[i]));
							break;
						}
					}
				//}
			}
		});
	} else if (command == "deleteAll") {
	}
})

/* The function that finds and returns the selected text */
var funcToInject = function() {
	var selection = window.getSelection();
	return (selection.rangeCount > 0) ? selection.toString() : '';
};

/* This line converts the above function to string
 * (and makes sure it will be called instantly)
 */
var jsCodeStr = ';(' + funcToInject + ')();';
/**
 * @Author: rcv!LuongTuong
 * @Date: 12/17/2014
 * @Param: textArr Array store product code after filter by parttern
 * @Description:  Add Item register to storageChrome using key shortcurt
 */
function addItemToStorageByHotKey(textArr) {
	var preCount = localStorage.length;
	var addCount = 0;
	reloadWebikeTabIdWindowALL();
	while (textArr.length) {
		var item = textArr.pop().toString();
		var flg = saveChanges(item);
		//Catch raise popup 1 time
		if (!flg) {
			break;
		}
		addCount = addCount + 1;
	}
	
	if (preCount + addCount > limit) {
		alert(over20productCodeMessage);
	}

	if (webikeTabId == 0) {
		redirectPage();
	}
	
	setItem();
}

/**
 * @Author: rcv!LuongTuong
 * @Date: 12/08/2014
 * @Description:  Add Item register to storageChrome
 */
function addItemToStorage(info, tab) {
	if (info.selectionText != "") {
		var preCount = localStorage.length;
		var addCount = 0;
		reloadWebikeTabIdWindowALL();
		var arrResult = checkRegExp(info.selectionText);
		while (arrResult.length) {
			var flg = saveChanges(arrResult.pop().toString());
			if (!flg) {
				break;
			}
			addCount = addCount + 1;
		}
		
		if (preCount + addCount > limit) {
			alert(over20productCodeMessage);
		}

		if (webikeTabId == 0) {
			redirectPage();
		}
		
		setItem();
	}
}


function setItem() {
	chrome.windows.getAll({populate: true}, function(windows) {
		var numWindows = windows.length;
		for (var i=0; i < numWindows; i++ ) {
			var win = windows[i];
			var numTabs = win.tabs.length;

			var exist = false;
			//純正見積ページを強制的に選択状態にする
			for (var j = 0; j < numTabs; j++) {
				var tab = win.tabs[j];

				var currentTabUrl = tab.url;
				if (tab.url.indexOf(webikePage) > -1) {
					synDataLocalToWebike();
					chrome.tabs.update(tab.id, {selected: true});
					exist = true;
					break;
				}
			}
			
			if (exist) {
				//画面がある場合
				chrome.windows.update(win.id, {focused : true}, function(windows) {
					chrome.tabs.executeScript(null, {
						file: "webikeInjectCode.js"
					}, function() {
						localStorage.setItem("itemData", "");
						//all del
						for (var i = 1; i < limit; i++) {
							localStorage.removeItem(i);
						}
					});
				});
			}
		}
	});
}



function reloadWebikeTabIdWindowALL() {
	chrome.windows.getAll({populate: true}, function(windows) {
		var numWindows = windows.length;

		for (var i = 0; i < numWindows; i++) {
			var win = windows[i];
			var numTabs = win.tabs.length;

			for (var j = 0; j < numTabs; j++) {
				var tab = win.tabs[j];

				var currentTabUrl = tab.url;
				if (tab.url.indexOf(webikePage) > -1) {
					webikeTabId =  tab.id;
					break;
				}
			}
		}
		
		if (!webikeTabId) {
			webikeTabId = 0;
		}
	});
}


/**
 * @Author: rcv!LuongTuong
 * @Date: 12/08/2014
 * @Description:  Change tabs event
 */
//wata
chrome.tabs.onActivated.addListener(function(activeInfo) {
	localStorage.removeItem("webikeTabId");
	webikeTabId = 0;
	reloadWebikeTabIdWindowALL();
	chrome.tabs.get(activeInfo.tabId, function(tab) {
		console.log(tab.url + " " + tab.id);
		var currentTabUrl = tab.url;
		//loop url list

		chrome.contextMenus.removeAll();
		//Valid Url 
		main();
		//handle webike page, inject javascript
//		if ((currentTabUrl.indexOf(webikePage) > -1) && (webikeTabId == tab.id)) {
//			chrome.tabs.executeScript(null, {
//				file: "webikeInjectCode.js"
//			}, function() {
//				localStorage.setItem("itemData", "");
//				//all del
//				for (var i = 1; i < limit; i++) {
//					localStorage.removeItem(i);
//				}
//			});
//		}
		
		//end handle webike page
	});
});


/**
 * @Author: rcv!LuongTuong
 * @Date: 12/08/2014
 * @Description: Build default content. Load 1st enable add-on
 */
function main() {
	for (var i = 1; i < limit; i++) {
		localStorage.removeItem(i);
	}

	chrome.contextMenus.create({
		"title": contextMenuRegister,
		"contexts": ["all"],
		"onclick": addItemToStorage
	});
}

/**
 * @Author: rcv!LuongTuong
 * @Date: 12/08/2014
 * @Description: Reload and build context menu after register
 */
function reloadExtension() {
	//Remove all context menu
	chrome.contextMenus.removeAll();
	// Create one test item for each context type.
	//When select text
	chrome.contextMenus.create({
		"title": contextMenuRegister,
		"contexts": ["all"],
		"onclick": addItemToStorage
	});
}


/**
 * @Author: rcv!LuongTuong
 * @Date: 12/08/2014
 * @Description:  handle param and redirect to webike site
 */
function redirectPage() {
	chrome.tabs.create({'url': webikePage, "selected":false});
	reloadWebikeTabIdWindowALL();
}

/**
 * @Author: rcv!LuongTuong
 * @Date: 12/08/2014
 * @Description: Register item
 * @param: value is code selected text
 */
function saveChanges(value) {
	// Get a value saved in a form.
	var theValue = value;
	// Check that there's some code there.
	if (!theValue) {
		return;
	}
	
	window.localStorage.setItem(localStorage.length + 1, theValue);

	return true;
}
	
/**
 * @Author: rcv!LuongTuong
 * @Date: 12/17/2014
 * @Description:  Delete all
 */
function deleteAllFunction() {
	var confirmFlag = confirm(deleteMessage);
	//Click cancel button
	if (!confirmFlag) {
		del = false;
		return;
	}

	del = true;
}

/**
 * @Author: rcv!LuongTuong
 * @Date: 12/08/2014
 * @Description: get all code from localStorage
 * @return: Array with code
 */
function getSaveChanges() {
	var resultStr = [];
	var generateId = localStorage.length;
	for (var i = 0; i < generateId; i++) {
		resultStr.push(localStorage.getItem(i + 1));
	}

	return resultStr;
}

// Intentionally create an invalid item, to show off error checking in the
// create callback.
console.log("About to try creating an invalid item - an error about " + "item 999 should show up");
chrome.contextMenus.create({
	"title": "Oops",
	"parentId": 999
}, function() {
	if (chrome.extension.lastError) {
		console.log("Got expected error: " + chrome.extension.lastError.message);
	}
});

/**
 * @Author: rcv!LuongTuong
 * @Date: 12/15/2014
 * @Description: filter text by partern
 * @return: Array with code
 */
function checkRegExp(textSelect) {
	var textPerform = textSelect;
	var arrResult = [];
	if (textPerform.indexOf(" ") != -1) {
		var textSelects = textPerform.split(" ");
		for (var i = 0; i < textSelects.length; i++) {
			var ret = false;
			for (var j = 0; j < regExpCommon.length; j++) {
				if (textSelects[i].match("^" + regExpCommon[j])) {
					var strResult = textSelects[i].match("^" + regExpCommon[j]);
					if (strResult != null && strResult != "") {
						ret = true;
						arrResult.push(strResult[0]);
						break;
					}
				}
			}
			
			if (!ret) {
				if (textSelects[i].length > 6 && textSelects[i].match(/^[a-zA-Z0-9]+$/)) {
					if ((textSelects[i].match(/[a-zA-Z]+/) && textSelects[i].match(/[0-9]+/))) {
						arrResult.push(textSelects[i]);
					}
				}
			}
		}

		if (arrResult.length == 0) {
			var arrTxt = textPerform.split(" ");
			for (var i = 0; i < arrTxt.length; i++) {
				if (arrTxt[i] != null) {
					if (arrTxt[i].length > 6 && arrTxt[i].match(/^[a-zA-Z0-9]+$/)) {
						//半角英数字で構成されている
						if ((arrTxt[i].match(/[a-zA-Z]+/) && arrTxt[i].match(/[0-9]+/))) {
							//英字、数字が使用されている
							arrResult.push(arrTxt[i]);
						}
					}
				}
			}
		}
	} else {
		//区切り文字がない場合
		for (var i = 0; i < regExpCommon.length; i++) {
			if (textPerform.match("^" + regExpCommon[i])) {
				var strResult = textPerform.match("^" + regExpCommon[i]);
				if (strResult != null && strResult != "") {
					arrResult.push(strResult[0]);
					textPerform = textPerform.replace(strResult[0], "");
					break;
				}
			}
		}
		if (arrResult.length == 0) {
			if (textSelect.length > 6 && textSelect.match(/^[a-zA-Z0-9]+$/)) {
				//半角英数字で構成されている
				if ((textSelect.match(/[a-zA-Z]+/) && textSelect.match(/[0-9]+/))) {
					//英字、数字が使用されている
					arrResult.push(textSelect);
				}
			}
		}
	}
	
	return arrResult;
}